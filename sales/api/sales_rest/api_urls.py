from django.urls import path
from .api_views import api_saleRecord, api_saleRecords

urlpatterns = [
    path("sales_rest/", api_saleRecord, name="api_saleRecord"),
    path("sales_rest/<int:pk>/", api_saleRecords, name="api_saleRecords")
]