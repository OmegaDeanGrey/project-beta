from django.shortcuts import render
from common.json import ModelEncoder
from .models import Customer, SalesPerson, AutomobileVO, SaleRecord
from .encoders import CustomerEncoder, SalesPersonEncoder, AutomobileVOEncoder, SaleRecordEncoder 
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
#from .acls import get_photo
import json
# Create your views here.

@require_http_methods(["GET", "POST"])
def api_saleRecords(request):
    if request.method == "GET":
        saleRecord = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_record": saleRecord},
            encoder=SaleRecordEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            vin = content["vin"]
            model = SaleRecord.objects.get(pk=vin)
            content["model"] = model #model=vin?
            auto = SaleRecord.objects.create(**content)
            return JsonResponse(
                auto,
                encoder=SaleRecordEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the sale record"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_saleRecord(request, vin):
    if request.method == "GET":
        try:
            auto = SaleRecord.objects.get(vin=vin)
            return JsonResponse(
                auto,
                encoder=SaleRecordEncoder,
                safe=False
            )
        except SaleRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = SaleRecord.objects.get(vin=vin)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=SaleRecordEncoder,
                safe=False,
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            auto = SaleRecord.objects.get(vin=vin)

            props = ["color", "year"] #props must reflect correct params?
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=SaleRecordEncoder,
                safe=False,
            )
        except SaleRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response








# @require_http_methods(["GET", "PUT", "DELETE"])
# def api_account_detail(request, email):
#     try:
#         account = User.objects.filter(is_active=True).get(email=email)
#     except User.DoesNotExist:
#         print("User.DoesNotExist", email)
#         if request.method == "GET":
#             response = JsonResponse({"message": email})
#             response.status_code = 404
#             return response
#         else:
#             account = None

#     if request.method == "GET":
#         return JsonResponse(
#             account,
#             encoder=AccountModelEncoder,
#             safe=False,
#         )
#     elif request.method == "PUT":
#         try:
#             content = json.loads(request.body)
#         except json.JSONDecodeError:
#             response = JsonResponse({"message": "Bad JSON"})
#             response.status_code = 400
#             return response

#         if "email" in content:
#             del content["email"]
#         if "username" in content:
#             del content["username"]
#         if account is not None:
#             for property in content:
#                 if property != "password" and hasattr(account, property):
#                     setattr(account, property, content[property])
#                 elif property == "password":
#                     account.set_password(content["password"])
#             status = 200
#             response_content = account
#         else:
#             status, response_content, account = create_user(request.body)
#         if account:
#             account.save()
#             send_account_data(account)
#         response = JsonResponse(
#             response_content,
#             encoder=AccountModelEncoder,
#             safe=False,
#         )
#         response.status_code = status
#         return response
#     else:
#         account.is_active = False
#         account.save()
#         send_account_data(account)
#         response = HttpResponse()
#         response.status_code = 204
#         return response