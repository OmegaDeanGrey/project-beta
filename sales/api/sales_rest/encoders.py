from common.json import ModelEncoder
from .models import SaleRecord, Customer, AutomobileVO, SalesPerson



class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "price",
        "salesperson",
        "customer",
        "automobile",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "name",
        "address",
        "phone_number",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "manufacturer"
        "VIN"
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "name",
        "employee_number",
    ]