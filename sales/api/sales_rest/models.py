from django.db import models

# Create your models here.
class SalesPerson(models.Model):
    name = models.CharField(max_length = 200)
    employee_number = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return f"{self.name}"


class Customer(models.Model):
    name = models.CharField(max_length = 200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=11) #default may be incorrect

    def __str__(self):
        return f"{self.name}"


class AutomobileVO(models.Model):
    manufacturer = models.CharField(max_length=200)
    VIN = models.CharField(max_length=17)

    def __str__(self):
        return f"{self.VIN}"

class SaleRecord(models.Model):
    price = models.CharField(max_length=20)
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sale_record",
        on_delete=models.CASCADE,null = True,
        blank = True,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customer",
        on_delete=models.CASCADE,null = True,
        blank = True,
    )
    salesperson = models.ForeignKey(
        SalesPerson,
        related_name="salesperson",
        on_delete=models.CASCADE,null = True,
        blank = True,
    )

    def __str__(self):
        return f"{self.automobile}"






