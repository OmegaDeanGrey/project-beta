import React from 'react';
import { renderMatches } from 'react-router-dom';

class saleRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobileVO: '',
            salesperson: '',
            customer: '',
            price: '',
        };
        this.handleAutomobileVOChange = this.handleManufacturerChange.bind(this);
        this.handleSalesPersonChange = this.handleModelNameChange.bind(this);
        this.handleCustomerChange = this.handleColorChange.bind(this);
        this.handlePriceChange = this.handlePictureUrlChange.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.saleRecordForm;
        const url = 'http://localhost:8090/api/sales_rest/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {   'Content-Type': 'application/json',  //Record a new sale?
        },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
        const newsaleRecord = await response.json();
        const cleared = {
            automobileVO: '',
            salesperson: '',
            customer: '',
            price: '',   
        };
        this.setState(cleared);
    }
    }

    handleAutomobileVOChange(event) {
        const value = event.target.value;
        this.setState({automobileVO: value})
    }

    handleSalesPersonChange(event) {
        const value = event.target.value;
        this.setState({salesperson: value})
    }

    handleCustomerChange(event) {
        const value = event.target.value;
        this.setState({customer: value})
    }

    handlePriceChange(event) {
        const value = event.target.value;
        this.setState({price: value})
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/inventory/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({bins: data.inventory}) //inventory?
        }
    }

    render() {
        return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Record a new sale</h1>
                <form onSubmit={this.handleSubmit} id="create-saleRecord-form">
                <div className="mb-3">
                    <select value={this.state.automobileVO} onChange={this.handleAutomobileVOChange} required name="automobileVO" id="automobileVO" className="form-select">
                    <option value="">Choose an Automobile</option>
                    {this.state.automobileVO.map(automobileVO => {
                        return (
                        <option key={automobileVO.id} value={automobileVO.href}>
                            {automobileVO.vin}
                        </option>
                        );
                    })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={this.state.salesperson} onChange={this.handleSalesPersonChange} required name="salesperson" id="salesperson" className="form-select">
                    <option value="">Choose a Sales Person</option>
                    {this.state.salesperson.map(salesperson => {
                        return (
                        <option key={salesperson.id} value={salesperson.href}>
                            {salesperson.employee_number} 
                        </option>
                        );
                    })}
                    </select>
                </div>
                <div className="mb-3">
                    <select value={this.state.customer} onChange={this.handleCustomerChange} required name="customer" id="customer" className="form-select">
                    <option value="">Choose a Customer</option>
                    {this.state.customer.map(customer => {
                        return (
                        <option key={customer.id} value={customer.href}>
                            {customer.customer}
                        </option>
                        );
                    })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <input value={this.state.price} onChange={this.handlepriceChange} required name="Price" required type="text" name="price" id="color" className="form-control" />
                    <label htmlFor="Price">Price</label>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
        );
    }
    }

    export default saleRecordForm; 

