import React, { useEffect } from 'react';
import App from './App';
import {useState} from 'react'


function SalesRecordList() {
    const [saleRecord, setsaleRecord] = useState([])

    const fetchsaleRecord = async () => {
        const url = 'http://localhost:8090/api/sales_rest/'
        const res = await fetch(url)
        const saleRecordJSON = await res.json()
        setShoes(saleRecordJSON.saleRecord)
    }
    useEffect(() => {
        fetchsaleRecord()
    }, [])

    function handleDelete(vin) { //id?
        const url = `http://localhost:8090/api/sales_rest/${vin}/`
        const fetchConfig = {method: 'DELETE'}
        const response = fetch(url, fetchConfig)
        setsaleRecord(saleRecord.filter(
            function(saleRecord) {
                return saleRecord.vin !== vin;
            }
        ))
        // find saleRecord by vin and remove from saleRecord array
    }

//key, salesperson.id is not what I want.   Ask about form-select.
    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>SalesPerson</th>
                <th>Customer</th>
                <th>VIN#</th>
                <th>Sales Price</th>
            </tr>
            </thead>
            <tbody>
            {saleRecord.map( saleRecord => {
                return (
                <tr key={salesperson.id}>  
                    <td>{ saleRecord.salesperson }</td>
                    <td>{ saleRecord.customer }</td>
                    <td>{ saleRecord.automobileVO.vin }</td>
                    <td>{ saleRecord.price }</td>
                    <td><button variant="outline-danger" onClick={() => handleDelete(saleRecord.vin)}>Delete</button></td>
                </tr>
                );
            })}
            </tbody>
        </table>
    );
}

export default SalesRecordList;